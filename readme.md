# What is this? #
A project for the course Web Services at Universitat Politècnica de Catalunya in Barcelona. 
We are taking the Instagram API and improving it by taking it to the level 3 of the Richardson Maturity Model. 

# What technologies are used? #

PHP(Laravel), Bootstrap and PostgreSQL.

# Visit the application at *http://damp-sierra-3292.herokuapp.com/* #