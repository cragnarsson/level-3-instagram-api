swagger: "2.0"
info:
  description: |
    This is a Level-3 API implementation of Instagram. This API provides extra functionality to the already existing level-2 Instagram API.

  version: "0.1.0"
  title: Instagram Level-3 API
  termsOfService: http://localhost/terms/
  contact:
    name: christian@localhost
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
host: localhost
basePath: /v1
schemes:
  - http
paths:
  /users/{userId}:
    get:
      tags:
        - user
      summary: Request user information
      description: "This endpoint will return basic user information given an accesstoken and user id."
      operationId: getUserById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: userId
          description: The user id that needs to be fetched.
          required: true
          type: string
      responses:
        "404":
          description: User not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/User"
  /users/{userId}/recent:
    get:
      tags:
        - user
      summary: Request user information
      description: "This endpoint will return a list of the media that is uploaded by this user."
      operationId: getMediaByUser
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: userId
          description: The user id  whose media needs to be fetched.
          required: true
          type: string
      responses:
        "404":
          description: User not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/User"
        "400":
          description: The user has no media.
  /users/{userId}/follow:
    post:
      tags:
        - user
      summary: Follow a given user
      description: "Follow a given user on Instagram provided his user id"
      operationId: followUser
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: userId
          description: The currently logged in user.
          required: true
          type: string
        - in: formData
          name: otherUserId
          description: The user id that needs to be followed.
          required: true
          type: string
      responses:
        "404":
          description: User to be followed not found
        "200":
          description: The user was successfully followed
          schema:
            $ref: "#/definitions/User"
  /users/{userId}/unfollow:
    post:
      tags:
        - user
      summary: Unfollow a given user
      description: "Unfollow a given user on Instagram provided his user id"
      operationId: unfollowUser
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: userId
          description: The currently logged in user.
          required: true
          type: string
        - in: formData
          name: otherUserId
          description: The user id that needs to be unfollowed.
          required: true
          type: string
      responses:
        "404":
          description: User to be followed not found
        "200":
          description: The user was successfully unfollowed
          schema:
            $ref: "#/definitions/User"
  /users/{userId}/followers:
    get:
      tags:
        - user
      summary: List of followers
      description: ""
      operationId: listOfFollowers
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: userId
          description: The currently logged in user.
          required: true
          type: string
      responses:
        "404":
          description: User to be followed not found
        "200":
          description: The user was successfully unfollowed
          schema:
            $ref: "#/definitions/User"
        "400":
          description: User has no followers
  /media/{mediaId}:
    get:
      tags:
        - media
      summary: Request media 
      description: "This endpoint will return basic media information given an accesstoken and media id."
      operationId: getMediaById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: mediaId
          description: The media id that needs to be fetched.
          required: true
          type: string
      responses:
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Media"
        "404":
          description: media not found
  /media/{mediaId}/likes:
    post:
      tags:
        - media
      summary: Request media 
      description: "This endpoint will post a like to the media specified by the media id."
      operationId: getMediaById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: mediaId
          description: The media id that will receive the like.
          required: true
          type: string
      responses:
        "404":
          description: Media not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Media"
  /media/{mediaId}/comments:
    post:
      tags:
        - media
      summary: Request media 
      description: "This endpoint will post a comment to the media specified by the media id."
      operationId: getMediaById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: mediaId
          description: The media id that will receive the comment.
          required: true
          type: string
      responses:
        "404":
          description: Media not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Media"  
  /media/{mediaId}/comments/{commentId}:
    delete:
      tags:
        - media
      summary: Request media 
      description: "This endpoint will delete a comment on the media specified by the media id."
      operationId: getMediaById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: mediaId
          description: The media id that will identify the media.
          required: true
          type: string
        - in: path
          name: commentId
          description: The comment id that will be deleted.
          required: true
          type: string
      responses:
        "404":
          description: Media not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Media"
  /media/{mediaId}/comments:
    get:
      tags:
        - media
      summary: Request media 
      description: "This endpoint will get all the comments for the media specified by the media id."
      operationId: getMediaById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: mediaId
          description: The media id that be used to identify the media.
          required: true
          type: string
      responses:
        "404":
          description: Media not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Media" 
  /location/{locationId}:
    get:
      tags:
        - location
      summary: Request location information
      description: "This endpoint will return basic location information and links to its media given an accesstoken and location id."
      operationId: getLocationById
      produces:
        - application/ld+json
      parameters:
        - in: path
          name: locationId
          description: The location id that needs to be fetched.
          required: true
          type: string
      responses:
        "404":
          description: Location not found
        "200":
          description: successful operation
          schema:
            $ref: "#/definitions/Location"
        "400":
          description: The location has no media.
securityDefinitions:
  api_key: 
    type: apiKey
    name: api_key
    in: header
  petstore_auth: 
    type: oauth2
    authorizationUrl: http://petstore.swagger.wordnik.com/api/oauth/dialog
    flow: implicit
    scopes:
      write_pets: modify pets in your account
      read_pets: read your pets
definitions:
  User:
    properties:
      id:
        type: string
      username:
        type: string
      fullName:
        type: string
      profilPicture:
        type: string
        description: Link to the users profile picture.
      bio:
        type: string
      website:
        type: string
        description: Link to users website.
      uploadCount: 
        type: integer
        format: int32
        description: Amount of media the user has uploaded.
      followers: 
        type: integer
        format: int32
        description: Amount of the followers the user has.
      followees: 
        type: integer
        format: int32
        description: Amount of users that is following the user.
  Comment:
    properties:
      id:
        type: string
      from:
        $ref: "#/definitions/User"
      date:
        type: string
        format: date-time
  Image:
    properties:
      url:
        type: string
      width:
        type: integer
        format: int64
      height:
        type: integer
        format: int64
  Media:
    properties:
      type:
        type: string
        description: Descirbes the media type (image or video)
      usersInMedia:
        type: array
        items:
          $ref: "#/definitions/User"
      filter:
        type: string
        description: Filter used for this media
      tags:
        type: array
        items:
          type: string
      comments:
        type: array
        items:
          $ref: "#/definitions/Comment"
      caption:
        type: string
      likes:
        type: array
        items:
          $ref: "#/definitions/User"
      date:
        type: integer
        format: int64
      link:
        type: string
      user:
        $ref: "#/definitions/User"
      id:
        type: integer
        format: int32
      image:
        $ref: "#/definitions/Image"
      location:
        $ref: "#/definitions/Location"
  Location:
    properties:
      id:
        type: string
      name:
        type: string
      latitude:
        type: integer
        format: int64
      longitude:
        type: integer
        format: int64
