<?php

include 'helpers/LevelThreeGenerators.php';

class MediaController extends BaseController{

/*
|--------------------------------------------------------------------------
| GET /media/search - Search for a media. 
| Parameters : 
| * lat = Latitude of the center search coordinate. If used, lng is required. 
| * lng = Longitude of the center search coordinate. If used, lat is required.
| * distance = Default is 1km(distance=1000), max distance is 5km. 
| * min_timestamp = A unix timestamp. All media returned will be taken later than this timestamp.
| * max_timestamp = A unix timestamp. All media returned will be taken earlier than this timestamp.
|--------------------------------------------------------------------------
|
*/
    public function searchMedia() 
    {   
        if (Input::has('lat') and Input::has('lng')) {
            Instagram::setAccessToken(User::getAccessToken());
            $media = Instagram::searchMedia(Input::get('lat'), Input::get('lng'),
            Input::get('distance'), Input::get('min_timestamp'), Input::get('max_timestamp'));

            return LevelThreeGenerators::generateLevelThreeAdvanced($media);
        } else {
         App::abort(403, 'Necessary parameters not included in request(lat & lng).');
        }
    }

/*
|--------------------------------------------------------------------------
| GET /media/popular - Get a list of what media is most popular at the moment. 
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function getPopular()
    {
        Instagram::setAccessToken(User::getAccessToken());
        $media = Instagram::getPopularMedia();

        return LevelThreeGenerators::generateLevelThreeAdvanced($media);
    }

/*
|--------------------------------------------------------------------------
| GET /media/{id} - Get a media resource by id. 
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function getMedia($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $media = Instagram::getMedia($id);

        return LevelThreeGenerators::generateLevelThreeAdvanced($media);
    }

/*
|--------------------------------------------------------------------------
| PUT /media/{id}/likes - Post a like to a media resource by id.
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function likeMedia($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $result = Instagram::likeMedia($id);

        return Response::make(json_encode($result, JSON_PRETTY_PRINT), $result->meta->code);
    }
/*
|--------------------------------------------------------------------------
| GET /media/{id}/likes - Get likes for a given media.
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function getMediaLikes($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $mediaLikes = Instagram::getMediaLikes($id);

        return LevelThreeGenerators::generateLevelThreeSimple($mediaLikes, false);
    }
/*
|--------------------------------------------------------------------------
| DELETE /media/{id}/likes - Delete a like for a given media.
| Parameter : id - The id of the media.
|--------------------------------------------------------------------------
|
*/
    public function deleteLikedMedia($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $result = Instagram::deleteLikedMedia($id);
        
        return Response::make(json_encode($result, JSON_PRETTY_PRINT), $result->meta->code);
    }
}