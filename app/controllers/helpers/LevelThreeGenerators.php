<?php 

class LevelThreeGenerators {
/*
|--------------------------------------------------------------------------
| This function generates level 3 response for advanced requests.
|--------------------------------------------------------------------------
|
*/
    public static function generateLevelThreeAdvanced($json_response)
    {
        // If the data is an array, we will loop through it.
        if(!isset($json_response->data))
        {
            return Response::make("<pre>" . json_encode($json_response,JSON_PRETTY_PRINT) . "</pre>",404);
        }
        if (is_array($json_response->data)) {
            foreach ($json_response->data as $entry) {
                LevelThreeGenerators::advancedGeneratorHelper($entry);
            }
        } else {
            // Else we will just send the object
            LevelThreeGenerators::advancedGeneratorHelper($json_response->data);
        }
        return Response::make(json_encode($json_response,JSON_PRETTY_PRINT),$json_response->meta->code);
    }

    private static function advancedGeneratorHelper($entry)
    {
        // For all the comments 
        foreach ($entry->comments->data as $comment) {
            LevelThreeGenerators::generatorUserHelper($comment->from);
        }
        // For all the likes
        foreach ($entry->likes->data as $like) {
            LevelThreeGenerators::generatorUserHelper($like);
        }
        // For all the users in the photo
        foreach ($entry->users_in_photo as $data) {
            LevelThreeGenerators::generatorUserHelper($data->user);
        }
        // For the caption
        if (isset($entry->caption->from)) {
            LevelThreeGenerators::generatorUserHelper($entry->caption->from);
        }
        // For the user
        LevelThreeGenerators::generatorUserHelper($entry->user);
        // For media
        LevelThreeGenerators::generatorMediaHelper($entry);
        // For the location
        if (isset($entry->location)) {
            LevelThreeGenerators::generatorLocationHelper($entry->location);
        }
    }

/*
|--------------------------------------------------------------------------
| This function generates level 3 response for simpler requests.
|--------------------------------------------------------------------------
|
*/
    public static function generateLevelThreeSimple($json_response, $location)
    {
        // If the data is an array, we will loop through it.
        if (!isset($json_response->data)) {
            return Response::json(NULL, 404);
        }
        if (is_array($json_response->data)) {
            foreach ($json_response->data as $entry) {
                LevelThreeGenerators::simpleGeneratorHelper($entry, $location);
            }
        } else { 
            // Else we will just send the object
            LevelThreeGenerators::simpleGeneratorHelper($json_response->data, $location);
        }
        return Response::make(json_encode($json_response,JSON_PRETTY_PRINT),$json_response->meta->code);
    }

    private static function simpleGeneratorHelper($entry, $location) {
        if ($location === true ) {
            LevelThreeGenerators::generatorLocationHelper($entry);
        } else {
            LevelThreeGenerators::generatorUserHelper($entry);
        }
    }

/*
|--------------------------------------------------------------------------
| These are functions that help the generators above.
|--------------------------------------------------------------------------
|
*/
    private static function generatorUserHelper($entry)
    {
        if (is_null($entry)) {
            return;
        }
        if (isset($entry->from)) {
            LevelThreeGenerators::generatorUserHelper($entry->from);
        } else {
            $entry->getUser = URL::route('getUser', $entry->id); 
            $entry->getRecentMedia = URL::route('recent', $entry->id);
            $entry->getFollowedBy = URL::route('followed-by', $entry->id);
            $entry->getFollows = URL::route('follows', $entry->id);
            // If the user is the authenticated user, we give self urls
            if (Session::get('authenticated_user') == $entry->id) {
                $entry->getFeed = URL::route('feed');
                $entry->getLiked = URL::route('liked');
            } else { // Otherwise, we give relationship urls
                $entry->postFollow = URL::route('follow', $entry->id);
                $entry->deleteUnfollow = URL::route('unfollow', $entry->id);
            }
            return $entry;
        }
    }

    private static function generatorMediaHelper($entry)
    {
        if (is_null($entry)) {
            return;
        }
        $entry->getSearch = URL::route('searchMedia');
        $entry->getPopular = URL::route('getPopular');
        $entry->getMedia = URL::route('getMedia', $entry->id);
        $entry->getMediaLikes = URL::route('getMediaLikes', $entry->id);
        $entry->postLikeMedia = URL::route('likeMedia', $entry->id);
        $entry->deleteLikedMedia = URL::route('deleteLikedMedia', $entry->id);
        $entry->getMediaComments = URL::route('getMediaComments', $entry->id);
        return $entry;
    }

    private static function generatorLocationHelper($entry)
    {
        if (is_null($entry)) {
            return;
        }
        if(isset($entry->latitude) && isset($entry->longitude)) {
            $location = "lat=". $entry->latitude . "&lng=" . $entry->longitude;
            $entry->getLocationSearch = URL::route('getLocationSearch', $location);
        }
        if (isset($entry->id)) {
            $entry->getLocation = URL::route('getLocation', $entry->id);
            $entry->getLocationMedia = URL::route('getLocationMedia', $entry->id);
        }
        return $entry;  
    }
}