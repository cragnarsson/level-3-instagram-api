<?php

include 'helpers/LevelThreeGenerators.php';

class LocationController extends BaseController {

/*
|--------------------------------------------------------------------------
| GET /locations/{id} - Get information about a location. 
| Parameter : id - The id of the location.
|--------------------------------------------------------------------------
|
*/
    public function getLocation($id)
    {
        Instagram::setAccessToken(User::getAccessToken());
        $location = Instagram::getLocation($id);

        return LevelThreeGenerators::generateLevelThreeSimple($location, true);
    }
/*
|--------------------------------------------------------------------------
| GET /locations/{id}/media/recent - Get a list of recent media objects from a given location. 
| Parameter : id - The id of the location.
|--------------------------------------------------------------------------
|
*/
    public function getMediaForLocation($id) {
        Instagram::setAccessToken(User::getAccessToken());
        $media = Instagram::getLocationMedia($id);

        return LevelThreeGenerators::generateLevelThreeAdvanced($media);
    }
/*
|--------------------------------------------------------------------------
| GET /locations/search - Search for a location by geographic coordinate. 
| Parameters : 
| * lat = Latitude of the center search coordinate. If used, lng is required. 
| * lng = Longitude of the center search coordinate. If used, lat is required.
| * distance = Default is 1km(distance=1000), max distance is 5km. 
|--------------------------------------------------------------------------
|
*/
    public function getlocationSearch() {
        if (Input::has('lat') and Input::has('lng')) {
            Instagram::setAccessToken(User::getAccessToken());
            $location = Instagram::searchLocation(Input::get('lat'), Input::get('lng'), Input::get('distance'));

            return LevelThreeGenerators::generateLevelThreeSimple($location, true);
        } else {
            App::abort(403, 'Necessary parameters not included in request(lat & lng).');
        }
    }
}