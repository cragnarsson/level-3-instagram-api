<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Level 3 Instagram API</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="jumbotron wrapper text-center">
		<h1>Level 3 Instagram API</h1>
		@yield('content')
	</div>
</body>
</html>
