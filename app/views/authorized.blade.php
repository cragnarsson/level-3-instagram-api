@extends('master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <h2>Available requests</h2>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <p class="panel-title">Users</p>
            </div>
            <div class="panel-body">     
                <div class="list-group">
                    <a href="/users/search/?q=Christian" class="list-group-item"><h4>Search for users - /users/search</h4></a>
                    <a href="/users/196232889" class="list-group-item"><h4>Get information about user - /users/{id}</h4></a>
                    <a href="/users/196232889/media/recent" class="list-group-item"><h4>Get recent media of user - /users/{id}/media/recent</h4></a>
                    <a href="/users/self/media/liked" class="list-group-item"><h4>See which media you liked - /users/self/media/liked</h4></a>
                    <a href="/users/self/feed" class="list-group-item"><h4>See your own feed - /users/self/feed</h4></a>
                    <a href="/users/196232889/followed-by" class="list-group-item"><h4>See who follows a user - /users/{id}/followed-by</h4></a>
                    <a href="/users/196232889/follows" class="list-group-item"><h4>See who a user follows - /users/{id}/follows</h4></a>
                </div>
             </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <p class="panel-title">Media</p>
            </div>
            <div class="panel-body">     
                <div class="list-group">
                    <a href="/media/search?lat=48.858844&lng=2.294351" class="list-group-item"><h4>Search for media - /media/search</h4></a>
                    <a href="/media/876842191855233263_468541737" class="list-group-item"><h4>Get information about media - /media/{id}</h4></a>
                    <a href="/media/popular" class="list-group-item"><h4>Get popular media - /media/popular</h4></a>
                    <a href="/media/876842191855233263_468541737/likes" class="list-group-item"><h4>Get likes for a media - /media/{id}/likes</h4></a>
                </div>
             </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <p class="panel-title">Comments</p>
            </div>
            <div class="panel-body">     
                <div class="list-group">
                    <a href="/media/876682764458015045_2158725/comments" class="list-group-item"><h4>Get comments for a media - /media/{id}/comments</h4></a>
                </div>
             </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <p class="panel-title">Locations</p>
            </div>
            <div class="panel-body">     
                <div class="list-group">
                    <a href="/locations/search?lat=48.8588443&lng=2.2943506" class="list-group-item"><h4>Search for media for a specific location  - /locations/search</h4></a>
                    <a href="/locations/2862169" class="list-group-item"><h4>Get location - /locations/{id}</h4></a>
                    <a href="/locations/2862169/media/recent" class="list-group-item"><h4>Get recent media for a location - /locations/{id}/media/recent</h4></a>
                </div>
             </div>
        </div>
    </div>
</div>
@stop